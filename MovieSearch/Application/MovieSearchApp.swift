//
//  MovieSearchApp.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 13/03/24.
//

import SwiftUI

@main
struct MovieSearchApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
