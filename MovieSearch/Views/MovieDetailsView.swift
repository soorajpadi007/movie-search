//
//  MovieDetailsView.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 15/03/24.
//

import SwiftUI

struct MovieDetailsView: View {
    @ObservedObject var selectedMovie: MovieEntity
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            let _ = print(selectedMovie.id)
            if let posterPath = selectedMovie.posterPath, let url = URL(string: "https://image.tmdb.org/t/p/w500\(posterPath)") {
                HStack {
                    Spacer()
                    AsyncImage(url: url)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 150, height: 225)
                        .cornerRadius(10)
                    Spacer()
                }
            }
            HStack {
                Text("Rating - \(selectedMovie.rating)")
                Spacer()
                Button {
                    selectedMovie.isFavorite = !selectedMovie.isFavorite
                    try? selectedMovie.managedObjectContext?.save()
                } label: {
                    Image(systemName: selectedMovie.isFavorite ? "star.slash.fill" : "star.fill")
                        .foregroundStyle(.yellow)
                }
            }
            Text("Movie ID: \(selectedMovie.id)")
            Text(selectedMovie.overview ?? "")
            Spacer()
        }
        .padding()
        .navigationBarTitleDisplayMode(.large)
        .navigationTitle(selectedMovie.title ?? "")
    }
}
