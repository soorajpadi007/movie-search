//
//  ContentView.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 13/03/24.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \MovieEntity.releaseDate, ascending: false)],
        animation: .default)
    private var movies: FetchedResults<MovieEntity>
    
    @State private var selectedMovie: MovieEntity?
    @State private var searchText = ""
    @State private var showFavoritesMoviesOnly = false

    var body: some View {
        NavigationSplitView {
            ZStack(alignment: .center) {
                List(selection: $selectedMovie) {
                    ForEach(movies, id: \.self) { movie in
                        HStack {
                            if let url = URL(string: "https://image.tmdb.org/t/p/w500\(movie.posterPath ?? "")") {
                                AsyncImage(url: url)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 50, height: 50)
                                    .cornerRadius(5)
                            }
                            Text(movie.title ?? "")
                                .padding(.leading, 5)
                        }
                        .accessibilityIdentifier("MyHStack")
                    }
                    if !showFavoritesMoviesOnly {
                        LazyVStack {
                            // A small height is needed for the LazyVStack to load the Color.
                            Color.clear
                                .frame(width: 0, height: 1, alignment: .bottom)
                                .onAppear {
                                    if searchText.isEmpty {
                                        MovieAPIManager.shared.fetchPopularMovies()
                                    }
                                }
                        }
                        .listRowSeparator(.hidden)
                    }
                }
                .accessibilityIdentifier("MoviesList")
                .searchable(text: $searchText)
                .onChange(of: searchText, initial: false, { oldValue, newValue in
                    movies.nsPredicate = newValue.isEmpty ? nil : NSPredicate(format: "title CONTAINS[c] %@", searchText)
                    MovieAPIManager.shared.searchMovies(query: newValue)
                })
                .toolbar {
                    ToolbarItem(placement: .bottomBar) {
                        Button(!showFavoritesMoviesOnly ? "Show Favorites" : "Hide Favorites") {
                            showFavoritesMoviesOnly = !showFavoritesMoviesOnly
                            movies.nsPredicate = showFavoritesMoviesOnly ? NSPredicate(format: "isFavorite = true") : nil
                        }
                        .accessibilityIdentifier("FavoritesButton")
                    }
                }
                
                if movies.isEmpty {
                    if showFavoritesMoviesOnly {
                        Text("No Favorites Movies.")
                            .accessibilityIdentifier("NoFavoritesMoviesLabel")
                    } else {
                        Text("No Movies.")
                            .accessibilityIdentifier("NoMoviesLabel")
                    }
                }
            }
        } detail: {
            if let selectedMovie {
                MovieDetailsView(selectedMovie: selectedMovie)
                    .accessibilityIdentifier("MovieDetailsView")
            } else {
                Text("Select a movie")
                    .accessibilityIdentifier("SelectAMovieLabel")
            }
        }
    }
}
