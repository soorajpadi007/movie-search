//
//  MovieResult.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 15/03/24.
//

import Foundation

struct MovieResult: Codable {
    let page: Int
    let results: [Movie]
}
