//
//  Movie.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 15/03/24.
//

import Foundation

struct Movie: Codable {
    let id: Int
    let title: String
    let overview: String
    let releaseDate: String
    let posterPath: String
    let rating: Double
    
    enum CodingKeys: String, CodingKey {
        case id, title, overview
        case releaseDate = "release_date"
        case posterPath = "poster_path"
        case rating = "vote_average"
    }
}
