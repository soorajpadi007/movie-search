//
//  MovieAPIManager.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 15/03/24.
//

import Foundation
import CoreData
import os.log

class MovieAPIManager {
    static let shared = MovieAPIManager()
    private let apiKey = "f4d7ed01a7f67e6b8580642031e19cac"
    private var currentPopularPage = 1
    let logger = Logger()
    
    // MARK: - Fetch Popular Movies
    func fetchPopularMovies() {
        guard let url = URL(string: "https://api.themoviedb.org/3/discover/movie?api_key=\(apiKey)&include_adult=false&include_video=false&language=en-US&page=\(currentPopularPage)&sort_by=popularity.desc") else {
            logger.log("Invalid API URL.")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                self.logger.log("No data found.")
                return
            }
            
            guard response.statusCode == 200 else {
                self.logger.log("Invalid response.")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let movieResult = try decoder.decode(MovieResult.self, from: data)
                
                // Save each movie to Core Data
                let context = PersistenceController.shared.container.viewContext
                movieResult.results.forEach { movie in
                    if !self.isMoviePersisted(movieID: movie.id, context: context) {
                        self.saveMovieToCoreData(movie: movie, context: context)
                    }
                }
                
                // Increment currentPopularPage for next page
                self.currentPopularPage += 1

            } catch {
                self.logger.error("Unable to decode movie objects \(error.localizedDescription)")
            }
        }.resume()
    }
    
    // MARK: - Search Movies
    func searchMovies(query: String) {
        guard let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: "https://api.themoviedb.org/3/search/movie?query=\(encodedQuery)&api_key=\(apiKey)&include_adult=false&include_video=false&language=en-US&sort_by=popularity.desc") else {
            logger.log("Invalid API URL.")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                self.logger.log("No data found.")
                return
            }
            
            guard response.statusCode == 200 else {
                self.logger.log("Invalid response.")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let movieResult = try decoder.decode(MovieResult.self, from: data)
                
                // Save each movie to Core Data
                let context = PersistenceController.shared.container.viewContext
                movieResult.results.forEach { movie in
                    if !self.isMoviePersisted(movieID: movie.id, context: context) {
                        self.saveMovieToCoreData(movie: movie, context: context)
                    }
                }
            } catch {
                self.logger.error("Unable to decode movie objects \(error.localizedDescription)")
            }
        }.resume()
    }
    
    // MARK: - Core Data
    
    func isMoviePersisted(movieID: Int, context: NSManagedObjectContext) -> Bool {
        let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", movieID)
        
        do {
            let count = try context.count(for: fetchRequest)
            return count > 0
        } catch {
            logger.error("Failed to fetch movie: \(error.localizedDescription)")
            return false
        }
    }
    
    func saveMovieToCoreData(movie: Movie, context: NSManagedObjectContext) {
        let movieEntity = MovieEntity(context: context)
        movieEntity.id = Int64(movie.id)
        movieEntity.title = movie.title
        movieEntity.overview = movie.overview
        movieEntity.releaseDate = movie.releaseDate
        movieEntity.posterPath = movie.posterPath
        movieEntity.rating = movie.rating
        movieEntity.isFavorite = false
        do {
            try context.save()
        } catch {
            logger.error("Failed to save movie: \(error.localizedDescription)")
        }
    }
}
