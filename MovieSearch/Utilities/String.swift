//
//  String.swift
//  MovieSearch
//
//  Created by Sooraj Padiillam on 15/03/24.
//

import Foundation

extension String {
    func caseInsensitiveHasPrefix(_ prefix: String) -> Bool {
        return range(of: prefix, options: [.anchored, .caseInsensitive]) != nil
    }
}
