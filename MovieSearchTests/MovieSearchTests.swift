//
//  MovieSearchTests.swift
//  MovieSearchTests
//
//  Created by Sooraj Padiillam on 13/03/24.
//

import XCTest
import CoreData
@testable import MovieSearch

final class MovieSearchTests: XCTestCase {
    var sut: MovieAPIManager!
    
    override func setUpWithError() throws {
        // Initialize the API manager instance
        sut = MovieAPIManager.shared
    }
    
    override func tearDownWithError() throws {
        // Clean up
        sut = nil
    }
    
    // MARK: - API Manager Tests
    
    func testFetchPopularMovies() throws {
        let expectation = XCTestExpectation(description: "Fetch popular movies")
        sut.fetchPopularMovies()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            // Check if popular movies are fetched successfully
            let context = PersistenceController.shared.container.viewContext
            let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()

            do {
                let movies = try context.fetch(fetchRequest)
                XCTAssertTrue(!movies.isEmpty, "Popular movies fetched successfully")
            } catch {
                XCTFail("Failed to fetch popular movies: \(error.localizedDescription)")
            }

            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSearchMovies() throws {
        let query = "Avengers"
        let expectation = XCTestExpectation(description: "Search movies for query: \(query)")
        sut.searchMovies(query: query)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            // Check if movies are fetched successfully for the search query
            let context = PersistenceController.shared.container.viewContext
            let fetchRequest: NSFetchRequest<MovieEntity> = MovieEntity.fetchRequest()
            
            do {
                let movies = try context.fetch(fetchRequest)
                XCTAssertTrue(!movies.isEmpty, "Movies fetched successfully for search query: \(query)")
            } catch {
                XCTFail("Failed to fetch movies for search query: \(query), \(error.localizedDescription)")
            }
            
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    // MARK: - Core Data Tests
    
    func testIsMoviePersisted() throws {
        let movieID = 123456
        let context = PersistenceController.shared.container.viewContext
        let movieEntity = MovieEntity(context: context)
        movieEntity.id = Int64(movieID)
        let isPersisted = sut.isMoviePersisted(movieID: movieID, context: context)
        XCTAssertTrue(isPersisted, "Movie with ID \(movieID) is persisted")
    }
}
