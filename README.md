# Movie Search

## Description

The Movie Search app allows users to browse and search for movies using the TMDb API. It provides a user-friendly interface for viewing movie details, marking favorites, and searching for specific movies.

## Features

- Browse popular movies
- Search for movies by title
- View movie details including title, poster, rating, and overview
- Mark movies as favorites

## Installation

To run the app locally, follow these steps:

1. Clone the repository to your local machine.
2. Open the project in Xcode.
3. Build and run the app on a simulator or a physical device.

_Note: The project used TMDB database, sometimes we need a VPN due to government restriction to the websites._


## Components
### ContentView
Displays a list of movies fetched from the TMDb API.
Includes search functionality for finding specific movies.
Allows users to mark movies as favorites.
### MovieDetailsView
Displays detailed information about a selected movie.
Allows users to toggle the favorite status of a movie.
### MovieAPIManager
Handles communication with the TMDb API for fetching popular movies and search results.
Manages the Core Data stack for storing and retrieving favorite movies.
### PersistenceController
Sets up the Core Data stack, including the managed object model and persistent store.
Provides a shared instance for use throughout the app.
